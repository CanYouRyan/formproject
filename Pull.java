
import java.util.ArrayList;
import java.util.List;
import javax.swing.JDialog;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ryanmoujaled
 */
public class Pull{
    private static String name;
    private static int age;
    private static int graduationYear;
    private static int StudentID;
    private String LetterGrade;
    private static float marks;
    private static String major;
    
    
    static RecordJDialog.Student newData;
    
    static ArrayList<RecordJDialog.Student> recordsArrayList = new 
        ArrayList<RecordJDialog.Student>();

    
    
    
    public static void studentSet(int id,String myName, int myAge, int myYear, 
            String grades, String majoroffered){
        StudentID = id;
        name = myName;
        age = myAge;
        graduationYear = myYear;
        marks = Float.parseFloat(grades);
        major = majoroffered;
        newData = new RecordJDialog.Student();
        initialize(id, myName, myAge, myYear, Float.parseFloat(grades),
                majoroffered);
        addToArrayList(recordsArrayList);
    }
    
    
     static void initialize(int id,String myName, int myAge, int myYear, float 
             grades, String majoroffered){
        
        newData.name = myName;
        newData.studentID = id;
        newData.age = myAge;
        newData.graduationYear = myYear;
        newData.marks = grades;
        newData.programString = majoroffered;
         
    }
    
    
     static void addToArrayList(List myArray){
        myArray.add(newData);
    }
    
    public static String studentGetName(){
        return name;
    }
    public static int studentGetAge(){
        return age;
    }
    public static int studentGetYear(){
        return graduationYear;
    }
    public static int studentGetID(){
        return StudentID;
    }
    public static float studentGetMarks(){
        return marks;
    }
    public static String studentGetMajor(){
        return major;
    }

   
enum Major{BA, CS, MIS, EE, CE, ME};
    
}
